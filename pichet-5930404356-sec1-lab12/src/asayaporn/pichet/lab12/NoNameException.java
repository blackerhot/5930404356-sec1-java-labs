package asayaporn.pichet.lab12;

import javax.swing.JOptionPane;

public class NoNameException extends Exception {
	/**
		 * 
		 */
	private static final long serialVersionUID = 8846897183972518350L;

	public NoNameException() {
		JOptionPane.showMessageDialog(null, "Name has not been entered.");
	}
}
