package asayaporn.pichet.lab12;

import javax.swing.JOptionPane;

public class NoAddressException extends Exception {

	public NoAddressException() {
		JOptionPane.showMessageDialog(null, "Address has not been entered.");
	}

}
