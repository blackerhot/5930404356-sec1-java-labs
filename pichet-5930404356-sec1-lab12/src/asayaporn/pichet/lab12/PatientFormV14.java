package asayaporn.pichet.lab12;

import java.awt.event.ActionEvent;
import java.time.format.DateTimeParseException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
/**
 * This program will add try catch to each input value
 * 
 * 
 * @author Pichet Asayaporn 5930404356 sec 1 
 *
 */
public class PatientFormV14 extends PatientFormV13 {

	
	private static final long serialVersionUID = -5434220225933977677L;

	public PatientFormV14(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV14 patientForm14 = new PatientFormV14("Patient Form V14");
		patientForm14.addComponents();
		patientForm14.addListeners();
		patientForm14.setFrameFeatures();

	}

	public void actionPerformed(ActionEvent e) {

		try {
			
			if (e.getSource() == super.okNewBut) {

				if (super.nameT.getText().isEmpty()) {
					try {
						throw new NoNameException();
					} catch (NoNameException e1) {

					}
				}
				if (!super.maleRadio.isSelected() && !super.femaleRadio.isSelected()) {
					try {
						throw new NoGenderException();
					} catch (NoGenderException e1) {

					}
				}
				if (super.textAddress.getText().isEmpty()) {
					try {
						throw new NoAddressException();
					} catch (NoAddressException e1) {

					}
				}
				if (super.birtdateT.getText().isEmpty()) {
					try {
						throw new NoBirthdateException();
					} catch (NoBirthdateException e1) {
						
					}
				}
				
	
				
			}

				super.actionPerformed(e);
			
		}catch (Throwable t){
			
			if(t instanceof NumberFormatException ){

				JOptionPane.showMessageDialog(null, "Please enter weight in double and height in int");
			
			}
			if(t instanceof DateTimeParseException){
				JOptionPane.showMessageDialog(null,
						"Please enter Birthdate in" + " the format DD.MM.YYYY ex. 22.02.2000");
			
			}
			
		} 
	}
}
