package asayaporn.pichet.lab12;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import asayaporn.pichet.lab10.PatientFormV12;
import asayaporn.pichet.lab4.Gender;
import asayaporn.pichet.lab4.Patient;

/**
 * This program will create menu open and save
 * menu save will save the recent value in object list to String
 * and menu open will open the file and add to recent oject list
 * 
 * @author Pichet Asayaporn 5930404356 sec1 
 *
 */
public class PatientFormV13 extends PatientFormV12 {

	private static final long serialVersionUID = 1379855288226292929L;
	private String name = "";
	private String birthDate = "";
	private String weight = "";
	private String height = "";
	private String gender = "";
	private int num = 0;

	public PatientFormV13(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV13 patientForm13 = new PatientFormV13("Patient Form V13");
		patientForm13.addComponents();
		patientForm13.addListeners();
		patientForm13.setFrameFeatures();

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		fileChooser = new JFileChooser();
		if (e.getSource() == super.menuOpen) {

			// show OpenDialog

			int returnVal = fileChooser.showOpenDialog(null);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();

				try {
					
					FileReader fr = new FileReader(file);
					BufferedReader r = new BufferedReader(fr);
					String line;

					while ((line = r.readLine()) != null) {
						num++;

						if (num == 1) {
							name = line.split("= ")[1];
						} else if (num == 2) {

							birthDate = line.split("= ")[1];
							String year = birthDate.split("-")[0];
							String month = birthDate.split("-")[1];
							String date = birthDate.split("-")[2];
							birthDate = date + "." + month + "." + year;

						} else if (num == 3) {
							weight = line.split("= ")[1];
						} else if (num == 4) {
							height = line.split("= ")[1];
						} else if (num == 5) {
							gender = line.split("= ")[1];

							Patient listPa = new Patient(name, birthDate,
									(gender.equals("MALE") ? Gender.MALE : Gender.FEMALE), Double.parseDouble(weight),
									Integer.parseInt(height));
							list.add(listPa);
							num = 0;

						}

					}

					r.close();
					fr.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "You have opened file " + file.getName());
			} else if (returnVal == JFileChooser.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Open command is cancelled");
			}

		} else if (e.getSource() == super.menuSave) {

			// show SaveDialog

			int returnVal = fileChooser.showSaveDialog(null);

			if (returnVal == JFileChooser.APPROVE_OPTION) {

				File file = fileChooser.getSelectedFile();
				
				try {
					
					PrintWriter p = new PrintWriter(file);
					for (int i = 0; i < list.size(); i++) {
						p.println("Name = " + list.get(i).getName());
						p.println("Birthdate = " + list.get(i).getBirthdate().toString());
						p.println("Weight = " + list.get(i).getWeight());
						p.println("Height = " + list.get(i).getHeight());
						p.println("Gender = " + list.get(i).getGender());
					}
					p.close();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "You have saved patient data to file " + file.getName());
			} else if (returnVal == JFileChooser.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Save Command is cancelled");
			}
		} else {
			super.actionPerformed(e);
		}

	}

}
