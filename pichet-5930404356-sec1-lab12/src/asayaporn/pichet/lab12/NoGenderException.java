package asayaporn.pichet.lab12;

import javax.swing.JOptionPane;

public class NoGenderException extends Exception {

	public NoGenderException(){
		JOptionPane.showMessageDialog(null, "No gender has been selected.");
	}
}
