package asayaporn.pichet.lab9;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * This program will extends from PatientFormV8
 * and add Action to open save exit and color custom 
 * when choose color all text will change color followed 
 * 
 * @author Pichet Asayaporn 593040435-6
 * 
 *
 */
public class PatientFormV9 extends PatientFormV8  {

	
	private static final long serialVersionUID = 8329614166698934410L;
	protected JFileChooser fileChooser;
	protected JColorChooser colorChooser;
	protected Color color = Color.black;

	public PatientFormV9(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV9 patientForm9 = new PatientFormV9("Patient Form V8");
		patientForm9.addComponents();
		patientForm9.addListeners();
		patientForm9.setFrameFeatures();

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		super.actionPerformed(e);

		fileChooser = new JFileChooser();
		colorChooser = new JColorChooser();

		
		if (e.getSource() == super.menuOpen) {
			
			//show OpenDialog 
			
			int returnVal = fileChooser.showOpenDialog(null);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				JOptionPane.showMessageDialog(null, "You have opened file " + file.getName());
			} else if (returnVal == JFileChooser.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Open command is cancelled");
			}
			

		} else if (e.getSource() == super.menuSave) {

			//show SaveDialog
			
			int returnVal = fileChooser.showSaveDialog(null);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fileChooser.getSelectedFile();
				JOptionPane.showMessageDialog(null, "You have saved file " + file.getName());
			} else if (returnVal == JFileChooser.CANCEL_OPTION) {
				JOptionPane.showMessageDialog(null, "Save Command is cancelled");
			}
			

		} else if (e.getSource() == super.menuExit) {
			//exit program
			System.exit(0);

		} else if (e.getSource() == super.cCustom) {
			
			//Show dialog color and set default color to display
			color=colorChooser.showDialog(this, "Select a color", color);
			
			nameT.setForeground(color);
			birtdateT.setForeground(color);
			weightT.setForeground(color);
			heightT.setForeground(color);
			textAddress.setForeground(color);
		}

	}

	@Override
	public void addListeners() {
		super.addListeners();
		
		//addActionListner for open exit and save
		
		super.menuOpen.addActionListener(this);
		super.menuExit.addActionListener(this);
		super.menuSave.addActionListener(this);
	

	}


}
