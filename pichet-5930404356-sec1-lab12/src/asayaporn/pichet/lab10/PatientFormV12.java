package asayaporn.pichet.lab10;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import asayaporn.pichet.lab4.Patient;

/**
 * This program extends from PatientFormV11
 * add 2 function remove and search of both input is name
 *  
 * 
 * @author Pichet Asayaporn 593040435-6
 *
 */
public class PatientFormV12 extends PatientFormV11 {

	private static final long serialVersionUID = 3309343028168120102L;

	public PatientFormV12(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV12 patientForm12 = new PatientFormV12("Patient Form V12");
		patientForm12.addComponents();
		patientForm12.addListeners();
		patientForm12.setFrameFeatures();

	}

	@Override
	public void addListeners() {
		super.addListeners();
		serchMI.addActionListener(this);
		removeMI.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		if (e.getSource() == serchMI) {
			String searchName = JOptionPane.showInputDialog("Please enter the patient name");
			//checking that name is found and get object back
			if (serchPatient(searchName) != null) {
				JOptionPane.showMessageDialog(null, serchPatient(searchName));
			}

		} else if (e.getSource() == removeMI) {
			String removeName = JOptionPane.showInputDialog("Please enter the patient name to remove");
			removePatient(removeName);
		}
	}

	//remove a patient by inputing a name and search 
	//then display all Patient left
	public void removePatient(String removeName) {
		Patient patient;
		if ((patient = serchPatient(removeName)) != null) {
			list.remove(patient);
			displayPatients();
		}
	}

	//search a name and then return a object of that name
	//if its not found then display and return null
	
	public Patient serchPatient(String searchName) {
		for (int i = 0; i < list.size(); i++) {

			Patient patientList = list.get(i);
			if (patientList.getName().equals(searchName)) {

				return patientList;

			}

		}

		JOptionPane.showMessageDialog(null, searchName + " is not found");
		return null;

	}

}
