package asayaporn.pichet.lab10;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import asayaporn.pichet.lab4.Gender;
import asayaporn.pichet.lab4.Patient;
import asayaporn.pichet.lab9.PatientFormV9;

/**
 * This program extends from PAtientFormV9 when user fill a information then
 * press ok button all information have been added to arrayList by passing
 * object Patient form lab4 then when you press display all information will
 * display by object toString
 * 
 * @author Pichet Asayaporn 593040435-6
 *
 */
public class PatientFormV10 extends PatientFormV9 {

	private static final long serialVersionUID = 1810621420042711495L;
	protected JMenu supData;
	protected JMenuItem displayMI, sortMI, serchMI, removeMI;
	protected ArrayList<Patient> list = new ArrayList<Patient>();

	public PatientFormV10(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV10 patientForm10 = new PatientFormV10("Patient Form V10");
		patientForm10.addComponents();
		patientForm10.addListeners();
		patientForm10.setFrameFeatures();

	}

	@Override
	public void addComponents() {
		super.addComponents();
		// initialize all menu and suMenu then add to menu
		supData = new JMenu("Data");
		displayMI = new JMenuItem("Display");
		sortMI = new JMenuItem("Sort");
		serchMI = new JMenuItem("Serch");
		removeMI = new JMenuItem("Remove");

		supData.add(displayMI);
		supData.add(sortMI);
		supData.add(serchMI);
		supData.add(removeMI);
		super.menuBar.add(supData);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);

		if (e.getSource() == super.okNewBut) {
			addPatient();

		} else if (e.getSource() == displayMI) {
			displayPatients();

		}

	}

	@Override
	public void addListeners() {
		super.addListeners();
		
		displayMI.addActionListener(this);
	}

	//add information by passing object Patient form lab4 and add to arrayList
	public void addPatient() {

		Patient listPa = new Patient(super.nameT.getText(), super.birtdateT.getText(),
				(maleRadio.isSelected() ? Gender.MALE : Gender.FEMALE), Double.parseDouble(super.weightT.getText()),
				Integer.parseInt(super.heightT.getText()));
		list.add(listPa);

	}

	//Displaying all list of Patient
	public void displayPatients() {
		String patientStr = "";
		for (int i = 0; i < list.size(); i++) {

			patientStr = patientStr + (i + 1) + ": " + list.get(i) + "\n";
		}

		JOptionPane.showMessageDialog(null, patientStr);
	}

}
