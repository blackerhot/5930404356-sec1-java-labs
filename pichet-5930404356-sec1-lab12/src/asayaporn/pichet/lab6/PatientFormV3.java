package asayaporn.pichet.lab6;

import javax.swing.*;
import java.awt.*;

public class PatientFormV3 extends PatientFormV2 {
	/**
	 * This programe extended PatientFromV2 and insert Type of patients and 2
	 * Menu in menubar
	 * 
	 * @author Pichet Asayaporn 5930404356
	 */
	private static final long serialVersionUID = 8614703442640203559L;

	protected JPanel panelSplit, panelEast, panelWest;
	protected JLabel label;
	protected JMenu menuFile,menuConfig;
	protected JMenuBar menuBar;
	protected JMenuItem menuNew,menuExit,menuSave,menuOpen,menuColor,menuSize;
	protected JComboBox<String> combo;
	String[] comboList = new String[] { "Inpatient", "Outpatient" };

	public PatientFormV3(String string) {
		super(string);

	}

	public static void createAndShowGUI() {
		PatientFormV3 patientForm3 = new PatientFormV3("Patient Form V3");
		patientForm3.addComponents();
		patientForm3.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {

		super.addComponents(); // caliing a super method
		GridBagConstraints gbc = new GridBagConstraints();
		setLayout(new GridBagLayout());

		menuBar = new JMenuBar();
		menuFile = new JMenu("File");

		menuBar.add(menuFile);

		

		menuNew= new JMenuItem("New");
		menuFile.add(menuNew);

		menuOpen = new JMenuItem("Open");
		menuFile.add(menuOpen);

		menuSave = new JMenuItem("Save");
		menuFile.add(menuSave);

		menuExit = new JMenuItem("Exit");
		menuFile.add(menuExit);

		menuConfig = new JMenu("Config");
		menuBar.add(menuConfig);
		menuColor = new JMenuItem("Color");
		menuConfig.add(menuColor);

		menuSize = new JMenuItem("Size");
		menuConfig.add(menuSize);

		menuBar.setPreferredSize(new Dimension(410, 20)); // set size of menuBar

		label = new JLabel("Type:");

		combo = new JComboBox<String>(comboList);
		combo.setPreferredSize(new Dimension(170, 20)); // set size of combobox
		combo.setSelectedIndex(1);

		// creating a new panel for split label and combo be west and east
		panelSplit = new JPanel();
		panelEast = new JPanel();
		panelWest = new JPanel();

		panelEast.setLayout(new GridLayout(1, 0, 0, 5));
		panelWest.setLayout(new GridLayout(1, 0, 0, 5));
		panelEast.add(combo);
		panelWest.add(label);

		// set panelsplit by Borderlayout
		panelSplit.setLayout(new BorderLayout(100, 0));
		panelSplit.add(panelWest, BorderLayout.WEST);
		panelSplit.add(panelEast, BorderLayout.EAST);

		// setting cordinate gridbag
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(menuBar, gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(panelSplit, gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(panelGroup, gbc);
		gbc.gridx = 0;
		gbc.gridy = 3;
		add(panelmsw, gbc);

	}

}
