package asayaporn.pichet.lab5;

import asayaporn.pichet.lab4.Gender;

public class AccidentPatientV2 extends PatientV3 {

	String typeOfAccident;
	boolean isInICU;

	public AccidentPatientV2(String string, String string2, Gender male, double d, int i, String string3, boolean b) {
		super(string, string2, male, d, i);
		this.typeOfAccident = string3;
		this.isInICU = b;
	}

	public String getTypeOfAccident() {
		return typeOfAccident;
	}

	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}

	public boolean isInICU() {
		return isInICU;
	}

	public void setInICU(boolean isInICU) {
		this.isInICU = isInICU;
	}

	public void patientReport() {
		System.out.println("You were in an accident.");
	}

	@Override
	public String toString() {
		return "AccidentPatient [" + typeOfAccident + ", " +

				(isInICU ? "In ICU" : "Not in ICU.") + "], Patient [" + getName() + "," + getBirthdate() + ", "
				+ getGender() + ", " + getWeight() + " kg., " + getHeight() + " cm.]";
	}

	interface HasInsurance {

	}

	interface UnderLegalAge {

	}

	public void askPermission() {
		System.out.println("ask parents for permission to cure him in an accident.");

	}

	public void seeDoctor() {
		System.out.println("go see the doctor.");

	}

	public void pay() {
		System.out.println("pay the accident bill with insurrance");

	}

}
