package asayaporn.pichet.lab5;

import asayaporn.pichet.lab4.Gender;

public class VIPPatientV2 extends PatientV3 {

	private double totalDonation;

	private String passPhrase;

	public VIPPatientV2(String string, String string2, Gender female, double d, int i, int j, String string3) {
		super(encrypt(string), string2, female, d, i);

		this.totalDonation = j;
		this.passPhrase = string3;

	}

	@Override
	public String toString() {
		return "VIPPatient [" + totalDonation + ",Patient [" + getName() + "," + getBirthdate() + ", " + getGender()
				+ ", " + getWeight() + " kg. , " + getHeight() + " cm.]";
	}

	public double getTotalDonation() {
		return totalDonation;
	}

	public void setTotalDonation(double totalDonation) {
		this.totalDonation = totalDonation;
	}

	public double donate(double donate) {
		return this.totalDonation = donate;
	}

	// encrypt the Ascii code of a-z
	private static String encrypt(String message) {
		StringBuilder s = new StringBuilder();
		char[] c = new char[message.length()];

		for (int i = 0; i < message.length(); i++) {
			c[i] = message.charAt(i);

			c[i] = (char) ((((int) c[i] + 7) - (int) 'a') % 26 + (int) 'a');

			s.append(c[i]);

		}

		return s.toString();
	}

	// decrypt the Ascii code of a-z
	private static String decrypt(String message) {

		StringBuilder s = new StringBuilder();
		char[] c = new char[message.length()];

		for (int i = 0; i < message.length(); i++) {
			c[i] = message.charAt(i);
			if (c[i] < 'h') {
				c[i] = (char) ((((int) c[i] - 7) - (int) 'a') + 26 + (int) 'a');
			} else {
				c[i] = (char) ((((int) c[i] - 7) - (int) 'a') % 26 + (int) 'a');
			}

			s.append(c[i]);

		}

		return s.toString();

	}

	// getVIPName to check the password that have passed
	public String getVIPName(String passPhrase) {

		if (passPhrase.equals(this.passPhrase)) {

			return decrypt(getName());
		} else {
			return ("Wrong passphrase, please try agian");
		}

	}

	public void patientReport() {
		System.out.println("Your record is private.");
	}

	interface HasInsurance {

	}

	interface UnderLegalAge {

	}

	public void playWithpet() {
		System.out.println("pay with pet.	");

	}

	public void feedPet() {
		System.out.println("feed the pet");

	}

	public void seeDoctor() {
		System.out.println("go see the doctor.");

	}

}
