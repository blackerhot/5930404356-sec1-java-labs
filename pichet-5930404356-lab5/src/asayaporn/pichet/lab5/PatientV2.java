package asayaporn.pichet.lab5;

import asayaporn.pichet.lab4.Gender;
import asayaporn.pichet.lab4.Patient;

public class PatientV2 extends Patient {

	public PatientV2(String passName, String passBirthdate, Gender passGender, double passWeight, int passHeight) {
		super(passName, passBirthdate, passGender, passWeight, passHeight);
		// TODO Auto-generated constructor stub
	}

	public void patientReport() {

		System.out.println("You need medical attention.");
	}

}
