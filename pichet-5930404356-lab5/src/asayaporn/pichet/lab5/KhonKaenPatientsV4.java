package asayaporn.pichet.lab5;

import asayaporn.pichet.lab4.Gender;

public class KhonKaenPatientsV4 {
	public static void main(String[] str) {
		PatientV2 piti = new AccidentPatient("piti","12.01.2000",Gender.MALE,65.5,169,"Car accident",true);
		PatientV2 weera = new TerminalPatient("weera","15.02.2000",Gender.MALE,72,172,"Cancer","01.01.2017");
		PatientV2 duangjai = new VIPPatient("duangjai","21.05.2001",Gender.FEMALE,47.5, 154,1000000,"mickeymouse");
		System.out.println(piti);
		System.out.println(weera);
		System.out.println(duangjai);
		((AccidentPatient) piti).setInICU(false);
		System.out.println(piti.getName()+(((AccidentPatient) piti).isInICU()?
				" is still in ICU. ":" is out of ICU."));
		
		weera.setWeight(70.0);
		System.out.println(weera.getName()+"'s new weight is " + weera.getWeight() );
		
		System.out.println("Encrypted name for VIP patient is " + duangjai.getName() + ".");
		System.out.println(((VIPPatient) duangjai).getVIPName("password"));
		System.out.println(((VIPPatient) duangjai).getVIPName("mickeymouse"));
		((VIPPatient) duangjai).donate(1000000.0);
		System.out.println("Total donation for " + ((VIPPatient) duangjai).getVIPName("mickeymouse")+ 
				" is " + ((VIPPatient) duangjai).getTotalDonation());
		piti.patientReport();
		weera.patientReport();
		duangjai.patientReport();
		  
		
		
		
	}


	

}
