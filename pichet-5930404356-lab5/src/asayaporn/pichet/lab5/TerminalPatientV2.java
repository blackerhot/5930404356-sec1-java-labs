package asayaporn.pichet.lab5;

import java.time.LocalDate;

import asayaporn.pichet.lab4.Gender;

public class TerminalPatientV2 extends PatientV3 {

	private String termincalDisease;
	private LocalDate firstDiagnosed;

	@Override
	public String toString() {
		return "TerminalPatient [" + termincalDisease + "," + firstDiagnosed + "], Patient [" + getName() + ","
				+ getBirthdate() + ", " + getGender() + ", " + getWeight() + " kg. , " + getHeight() + " cm.]";
	}

	public TerminalPatientV2(String string, String string2, Gender male, int i, int j, String string3, String string4) {
		super(string, string2, male, i, j);
		this.termincalDisease = string3;
		this.firstDiagnosed = LocalDate.parse(string4, getNormalFormatter()); // pass
																				// Localdate
																				// to
																				// Stringg
	}

	public String getTermincalDisease() {
		return termincalDisease;
	}

	public void setTermincalDisease(String termincalDisease) {
		this.termincalDisease = termincalDisease;
	}

	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}

	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}

	public void pateintReport() {
		System.out.println("You have terminal illness");
	}

	// interface
	interface HasInsurance {

	}

	interface UnderLegalAge {

	}

	public void pay(int i) {
		System.out.println("pay " + i + " baht for his hospital visit by insurance.");

	}

	public void askPremission(String string) {
		System.out.println("ask " + string + "for permisiion to treat with Chemo.");

	}

	public void seeDoctor() {
		System.out.println("go see the doctor.");

	}

}
