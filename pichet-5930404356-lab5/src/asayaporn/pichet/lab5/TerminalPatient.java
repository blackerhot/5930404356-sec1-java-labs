package asayaporn.pichet.lab5;

import java.time.LocalDate;

import asayaporn.pichet.lab4.Gender;

public class TerminalPatient extends PatientV2 {

	private String termincalDisease;
	private LocalDate firstDiagnosed;

	@Override
	public String toString() {
		return "TerminalPatient [" + termincalDisease + "," + firstDiagnosed + "], Patient [" + getName() + ","
				+ getBirthdate() + ", " + getGender() + ", " + getWeight() + " kg. , " + getHeight() + " cm.]";
	}

	public TerminalPatient(String string, String string2, Gender male, int i, int j, String string3, String string4) {
		super(string, string2, male, i, j);
		this.termincalDisease = string3;
		this.firstDiagnosed = LocalDate.parse(string4, getNormalFormatter()); // passLocaldate to String
	}

	public String getTermincalDisease() {
		return termincalDisease;
	}

	public void setTermincalDisease(String termincalDisease) {
		this.termincalDisease = termincalDisease;
	}

	public LocalDate getFirstDiagnosed() {
		return firstDiagnosed;
	}

	public void setFirstDiagnosed(LocalDate firstDiagnosed) {
		this.firstDiagnosed = firstDiagnosed;
	}

	public void pateintReport() {
		System.out.println("You have terminal illness");
	}

}
