package asayaporn.pichet.lab5;

import asayaporn.pichet.lab4.Gender;

public class OlderPatient {

	public static void main(String[] str) {

		PatientV2 piti = new AccidentPatient("piti", "12.01.2000", Gender.MALE, 65.5, 169, "Car accident", true);
		PatientV2 weera = new TerminalPatient("weera", "15.02.2000", Gender.MALE, 72, 172, "Cancer", "01.01.2017");
		PatientV2 duangjai = new VIPPatient("duangjai", "21.05.2001", Gender.FEMALE, 47.5, 154, 1000000, "mickeymouse");
		AccidentPatient petch = new AccidentPatient("petch", "15.10.1999", Gender.MALE, 68, 170, "Fire accident", true);
		isOlder(piti, weera);
		isOlder(piti, duangjai);
		isOlder(piti, petch);
		isOldest(piti, weera, duangjai);
		isOldest(piti, duangjai, petch);

	}

	public static void isOlder(PatientV2 patient1, PatientV2 patient2) {

		// checkolder by using compareTo
		int checkolder = patient1.getBirthdate().compareTo(patient2.getBirthdate());

		if (patient1 instanceof VIPPatient) {

			if (checkolder < 0) {
				System.out.println(((VIPPatient) patient1).getVIPName("mickeymouse") + " is older than "
						+ patient2.getName() + ".");
			} else if (checkolder > 0) {
				System.out.println(((VIPPatient) patient1).getVIPName("mickeymouse") + " is not older than "
						+ patient2.getName() + ".");
			}

		} else if (patient2 instanceof VIPPatient) {
			if (checkolder < 0) {
				System.out.println((patient1.getName()) + " is older than "
						+ ((VIPPatient) patient2).getVIPName("mickeymouse") + ".");
			} else if (checkolder > 0) {
				System.out.println((patient1.getName()) + " is not older than "
						+ ((VIPPatient) patient2).getVIPName("mickeymouse") + ".");
			}
		} else {
			if (checkolder < 0) {
				System.out.println((patient1.getName()) + " is older than " + patient2.getName() + ".");
			} else if (checkolder > 0) {
				System.out.println((patient1.getName()) + " is not older than " + patient2.getName() + ".");
			}
		}

	}

	public static void isOldest(PatientV2 patient1, PatientV2 patient2, PatientV2 patient3) {

		String vip1 = null;
		String vip2 = null;
		String vip3 = null;
		int checkolder;

		// checking by if patient instanceof VIPPateint then will pass value to
		// String vip

		if (patient1 instanceof VIPPatient) {
			vip1 = ((VIPPatient) patient1).getVIPName("mickeymouse");
			vip2 = patient2.getName();
			vip3 = patient3.getName();
		} else if (patient2 instanceof VIPPatient) {
			vip2 = ((VIPPatient) patient2).getVIPName("mickeymouse");
			vip1 = patient1.getName();
			vip3 = patient3.getName();

		} else if (patient3 instanceof VIPPatient) {
			vip3 = ((VIPPatient) patient3).getVIPName("mickeymouse");
			vip1 = patient1.getName();
			vip2 = patient2.getName();
		}

		// check older by easy if statement

		if (true) {
			checkolder = patient1.getBirthdate().compareTo(patient2.getBirthdate());

			if (checkolder < 0) {

				checkolder = patient1.getBirthdate().compareTo(patient3.getBirthdate());

				if (checkolder < 0) {

					System.out.println(vip1 + " is the oldest among " + vip1 + " , " + vip2 + " and " + vip3 + ".");

				}
			}

		}

		if (true) {
			checkolder = patient2.getBirthdate().compareTo(patient1.getBirthdate());

			if (checkolder < 0) {

				checkolder = patient2.getBirthdate().compareTo(patient3.getBirthdate());

				if (checkolder < 0) {

					System.out.println(vip2 + " is the oldest among " + vip1 + " , " + vip2 + " and " + vip3 + ".");

				}
			}

		}

		if (true) {
			checkolder = patient3.getBirthdate().compareTo(patient1.getBirthdate());

			if (checkolder < 0) {

				checkolder = patient3.getBirthdate().compareTo(patient2.getBirthdate());

				if (checkolder < 0) {

					System.out.println(vip3 + " is the oldest among " + vip1 + " , " + vip2 + " and " + vip3 + ".");
				}
			}

		}

	}

}