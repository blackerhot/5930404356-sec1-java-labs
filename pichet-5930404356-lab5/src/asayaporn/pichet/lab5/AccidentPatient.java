package asayaporn.pichet.lab5;

import asayaporn.pichet.lab4.Gender;

public class AccidentPatient extends PatientV2 {

	String typeOfAccident;
	boolean isInICU;

	// constructor overload
	public AccidentPatient(String string, String string2, Gender male, double d, int i, String string3, boolean b) {
		super(string, string2, male, d, i);
		this.typeOfAccident = string3;
		this.isInICU = b;
	}

	// getter setter
	public String getTypeOfAccident() {
		return typeOfAccident;
	}

	public void setTypeOfAccident(String typeOfAccident) {
		this.typeOfAccident = typeOfAccident;
	}

	public boolean isInICU() {
		return isInICU;
	}

	public void setInICU(boolean isInICU) {
		this.isInICU = isInICU;
	}

	public void patientReport() {
		System.out.println("You were in an accident.");
	}

	@Override
	public String toString() {
		return "AccidentPatient [" + typeOfAccident + ", " +

				(isInICU ? "In ICU" : "Not in ICU.") + "], Patient [" + getName() + "," + getBirthdate() + ", "
				+ getGender() + ", " + getWeight() + " kg., " + getHeight() + " cm.]";
	}

}
