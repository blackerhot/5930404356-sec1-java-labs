package asayaporn.pichet.lab8;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.SwingUtilities;

public class PatientFormV5 extends PatientFormV4 {

	/**
	 * Program will add a sup-menu and create Action each of sup-menu
	 * 
	 * @author Pichet Asayaporn
	 */
	private static final long serialVersionUID = 1760116823668563365L;
	protected JMenu supColor, supSize;
	protected JMenuItem s16, s20, s24, sCustom, cBlue, cRed, cGreen, cCustom;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV5 patientForm5 = new PatientFormV5("Patient Form V5");
		patientForm5.addComponents();
		patientForm5.addListeners();
		patientForm5.setFrameFeatures();
	}

	public PatientFormV5(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public void addComponents() {
		super.addComponents();
		// remove old menu
		menuConfig.remove(menuColor);
		menuConfig.remove(menuSize);

		// add MenuColor and sup-menu of their
		supColor = new JMenu("Color");
		supColor.add(cBlue = new JMenuItem("Blue"));
		supColor.add(cGreen = new JMenuItem("Green"));
		supColor.add(cRed = new JMenuItem("Red"));
		supColor.add(cCustom = new JMenuItem("Custom..."));
		menuConfig.add(supColor);

		// add MenuSize and sup-menu of their
		supSize = new JMenu("Size");
		s16 = new JMenuItem("16");
		supSize.add(s16);
		s20 = new JMenuItem("20");
		supSize.add(s20);
		s24 = new JMenuItem("24");
		supSize.add(s24);
		sCustom = new JMenuItem("Custom");
		supSize.add(sCustom);
		menuConfig.add(supSize);

	}

	protected void addListeners() {
		super.addListeners();

		// each of sup-menu add Action
		s16.addActionListener(this);
		s20.addActionListener(this);
		s24.addActionListener(this);
		sCustom.addActionListener(this);
		cBlue.addActionListener(this);
		cGreen.addActionListener(this);
		cRed.addActionListener(this);
		cCustom.addActionListener(this);
	}

	public void actionPerformed(ActionEvent event) {
		super.actionPerformed(event);

		// Object command to get a object that performed
		Object command = event.getSource();

		// For set a value that performed
		if (command == s16) {
			Font font = new Font("SansSerif", Font.BOLD, 16);
			nameT.setFont(font);
			birtdateT.setFont(font);
			weightT.setFont(font);
			heightT.setFont(font);
			textAddress.setFont(font);
		} else if (command == s20) {
			Font font = new Font("SansSerif", Font.BOLD, 20);
			nameT.setFont(font);
			birtdateT.setFont(font);
			weightT.setFont(font);
			heightT.setFont(font);
			textAddress.setFont(font);
		} else if (command == s24) {
			Font font = new Font("SansSerif", Font.BOLD, 24);
			nameT.setFont(font);
			birtdateT.setFont(font);
			weightT.setFont(font);
			heightT.setFont(font);
			textAddress.setFont(font);

		} else if (command == sCustom) {

		} else if (command == cBlue) {
			nameT.setForeground(Color.blue);
			birtdateT.setForeground(Color.blue);
			weightT.setForeground(Color.blue);
			heightT.setForeground(Color.blue);
			textAddress.setForeground(Color.blue);
		} else if (command == cGreen) {
			nameT.setForeground(Color.green);
			birtdateT.setForeground(Color.green);
			weightT.setForeground(Color.green);
			heightT.setForeground(Color.green);
			textAddress.setForeground(Color.green);
		} else if (command == cRed) {
			nameT.setForeground(Color.RED);
			birtdateT.setForeground(Color.RED);
			weightT.setForeground(Color.RED);
			heightT.setForeground(Color.RED);
			textAddress.setForeground(Color.RED);
		} else if (command == cCustom) {

		}
	}

}
