package asayaporn.pichet.lab8;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;

import asayaporn.pichet.lab6.PatientFormV3;

public class PatientFormV4 extends PatientFormV3 implements ActionListener, ItemListener {

	/**
	 * Program will display all text by press OKbutton when press cancel all
	 * text will be reseted and when you select a type of gender and type of
	 * patient program will display dialog too.
	 * 
	 * 
	 * @author Pichet Asayaporn
	 */
	private static final long serialVersionUID = -697362195473561063L;

	protected JDialog showDialog = new JDialog();

	public static void createAndShowGUI() {
		PatientFormV4 patientForm4 = new PatientFormV4("Patient Form V4");
		patientForm4.addComponents();
		patientForm4.addListeners();
		patientForm4.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public PatientFormV4(String string) {
		super(string);

	}

	protected void addListeners() {

		// add Action for each object
		okBut.addActionListener(this);
		cancelBut.addActionListener(this);
		combo.addActionListener(this);

		// add raidoButton by ItemListener
		maleRadio.addItemListener(this);
		femaleRadio.addItemListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// object command for get a object that performing
		Object command = e.getSource();

		// checking type each of object then set action for each of them
		if (command == okBut) {

			JOptionPane.showMessageDialog(null,
					" Name = " + nameT.getText() + " Birthdate = " + birtdateT.getText() + " Weight = "
							+ weightT.getText() + " Height = " + heightT.getText() + "\n" + " Gender = "
							+ (maleRadio.isSelected() ? "Male" : "Female") + "\n" + " Address = "
							+ textAddress.getText() + "\n" + " Type = " + combo.getSelectedItem());
		} else if (command == cancelBut) {
			nameT.setText("");
			birtdateT.setText("");
			weightT.setText("");
			heightT.setText("");
			textAddress.setText("");

		} else if (command == combo) {
			// show mesaageDialog
			JOptionPane.showMessageDialog(null, "Your patient type is now changed to " + combo.getSelectedItem());
		}

	}

	public void itemStateChanged(ItemEvent e) {

		// object checkObject for get a object that performing
		Object checkObject = e.getSource();

		// checking that object is JRadioButton
		if (checkObject instanceof JRadioButton) {

			// checking JRadioButton is selected
			if (((JRadioButton) checkObject).isSelected()) {

				// create a text to JOptionPane but not display now
				JOptionPane pane = new JOptionPane(
						"Your gender type is now changed to " + ((JRadioButton) checkObject).getText());

				// creatDialog and set Title of pane and add to JDialog
				showDialog = pane.createDialog("Gender info");
				Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
				showDialog.pack();
				int w = getSize().width;
				int h = getSize().height;
				int x = (dim.width - w) / 2;
				int y = (dim.height - h) / 2 + this.getSize().height + 20;

				showDialog.setLocation(x, y);
				showDialog.setVisible(true);
			}
		}

	}

}
