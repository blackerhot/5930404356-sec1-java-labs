package asayaporn.pichet.lab6;

import java.awt.*;
import javax.swing.*;

public class PatientFormV2 extends PatientFormV1 {
	/**
	 * This program extended from PatientFormV1
	 * and will add JradioButton male female and Textarea of address
	 * 
	 * @author Pichet Asayaporn 5930404356
	 * 
	 */
	private static final long serialVersionUID = -424256430292833806L;
	protected JPanel panelGender, panelGroup;
	protected JLabel genderLabel, addressLabel;
	protected JTextArea textAddress;
	protected JRadioButton maleRadio, femaleRadio;
	protected JScrollPane scroll;
	protected ButtonGroup button;

	public PatientFormV2(String string) {
		super(string);
	}

	public static void createAndShowGUI() {
		PatientFormV2 patientForm2 = new PatientFormV2("Patient Form V2");
		patientForm2.addComponents();
		patientForm2.setFrameFeatures();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		
		super.addComponents(); // call a super method
		GridBagConstraints gbc = new GridBagConstraints(); // use GridBag
		setLayout(new GridBagLayout());

		panelGender = new JPanel();
		panelGroup = new JPanel();

		textAddress = new JTextArea(2,35);
		textAddress.setLineWrap(true); // set white line
		textAddress.setWrapStyleWord(true); // set word that have meaning
		textAddress.setText("Department of Computer Engineering,"
				+ " Faculty of Engineering, Khon Kaen University,"
				+ " Mittraparp Rd., T. Naimuang, A. Muang, Khon Kaen, Thailand, 40002");
		
		scroll = new JScrollPane(textAddress);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS); // use only scrollbar on vertical
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		genderLabel = new JLabel("Gender:");
		addressLabel = new JLabel("Address:");
		maleRadio = new JRadioButton("Male");
		femaleRadio = new JRadioButton("Female");
		
		//Group button by using ButtonGroup for selected only one
		button = new ButtonGroup();
		button.add(maleRadio);
		button.add(femaleRadio);

		panelGender.add(maleRadio);
		panelGender.add(femaleRadio);
		
		//add label and JRadio to Label and Text and increasing Gridlayout 5 ---> 6
		panelLabel.setLayout(new GridLayout(6, 1, 0, 5));
		panelText.setLayout(new GridLayout(6, 1, 0, 5));
		
		panelLabel.add(genderLabel);
		panelLabel.add(addressLabel);

		panelText.add(panelGender);

		
		panelGroup.setLayout(new BorderLayout(0, 0));
		panelGroup.add(panelLabel, BorderLayout.WEST);
		panelGroup.add(panelText, BorderLayout.EAST);
		panelGroup.add(scroll, BorderLayout.SOUTH);

		//set condinate gridbag
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(panelGroup, gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(panelmsw, gbc);

	}

}
