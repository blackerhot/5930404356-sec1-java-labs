package asayaporn.pichet.lab10;

import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.SwingUtilities;

import asayaporn.pichet.lab4.Patient;
/**
 * This program extends from PatientFormV10
 * and sorting all Patient by compare a weight ascending(least to most)
 * 
 * @author PICHERR
 *
 */
public class PatientFormV11 extends PatientFormV10 {


	private static final long serialVersionUID = 5878490778800737237L;

	public PatientFormV11(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV11 patientForm11 = new PatientFormV11("Patient Form V11");
		patientForm11.addComponents();
		patientForm11.addListeners();
		patientForm11.setFrameFeatures();

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		super.actionPerformed(e);
		if (e.getSource() == sortMI) {
			sortPatients();
			displayPatients();
		}
	}

	//sorting a patient compare a weight from least to most
	public void sortPatients() {
		
		list.sort(new Comparator<Patient>() {
			@Override
			public int compare(Patient p0, Patient p1) {
				if (p0.getWeight() > p1.getWeight()) {
					return 1;
				} else {
					return -1;
				}

			}
		});
	
	}

	@Override
	public void addListeners() {
		super.addListeners();
		sortMI.addActionListener(this);
	}

}
