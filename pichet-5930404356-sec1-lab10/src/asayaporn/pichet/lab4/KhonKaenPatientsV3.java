package asayaporn.pichet.lab4;

public class KhonKaenPatientsV3 {

	/**
	 * 
	 * KhonKaenPatientsV3
	 * 
	 * This program is to illustrate the example of how to create objects in
	 * class Patient , how to use static method , overloading constructor
	 * 
	 * @author Pichet Asayaporn
	 *
	 * @Version 1.0
	 *
	 *
	 */

	public static void main(String[] args) {
		Patient manee = new InPatient("Menee", "01.12.1980", Gender.FEMALE, 60, 150, "20.01.2017", "29.01.2017");
		Patient mana = new OutPatient("Mana", "22.04.1981", Gender.MALE, 70, 160, "23.01.2017");
		Patient chujai = new Patient("Chujai", "03.03.1980", Gender.FEMALE, 41.5, 175);

		System.out.println(manee);
		System.out.println(mana);
		System.out.println(chujai);
		if (isTaller(manee, mana)) {
			System.out.println(manee.getName() + " is taller than " + mana.getName());
		} else {
			System.out.println(mana.getName() + " is taller than " + manee.getName());
		}

	}

	// method boolean include "static" for using with static void main
	// use if else for compare who are taller .
	private static boolean isTaller(Patient manee, Patient mana) {

		if (manee.getHeight() > mana.getHeight())

			return true;

		else
			return false;
	}

}
