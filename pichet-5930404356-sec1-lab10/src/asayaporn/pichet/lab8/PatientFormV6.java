package asayaporn.pichet.lab8;

import java.awt.Dimension;
import java.awt.GridBagConstraints;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * This program will add image to JmenuItem and add imagePanel to a old panel
 * and set a real size of JmenuBar match with imagePanel
 * 
 * @author Pichet Asayaporn
 */
public class PatientFormV6 extends PatientFormV5 {

	private static final long serialVersionUID = 4706259799093299457L;

	protected JLabel labelImg;
	protected JPanel panelImg;
	protected ImageIcon img;
	protected int realWidth;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV6 patientForm6 = new PatientFormV6("Patient Form V6");
		patientForm6.addComponents();
		patientForm6.addListeners();
		patientForm6.setFrameFeatures();

	}

	public PatientFormV6(String string) {
		super(string);

	}

	@Override
	public void addComponents() {
		super.addComponents();
		// add icon to menu
		menuOpen.setIcon(new ImageIcon("openIcon.png"));
		menuExit.setIcon(new ImageIcon("quitIcon.png"));
		menuSave.setIcon(new ImageIcon("saveIcon.png"));
		// read image from folder
		img = new ImageIcon("manee.jpg");
		labelImg = new JLabel(img, JLabel.CENTER);

		// match the width of each panel
		if (img.getIconWidth() <= 400) {
			realWidth = 400;
		} else {
			realWidth = 400 + img.getIconHeight();
		}
		// set realWidth
		labelImg.setPreferredSize(new Dimension(realWidth, img.getIconHeight() + 50));
		menuBar.setPreferredSize(new Dimension(realWidth + 10, 20));

		// insert a lebelImmg to old panel
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		add(menuBar, gbc);
		gbc.gridx = 0;
		gbc.gridy = 3;
		add(panelSplit, gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		add(panelGroup, gbc);
		gbc.gridx = 0;
		gbc.gridy = 4;
		add(panelmsw, gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		add(labelImg, gbc);

	}

}
