package asayaporn.pichet.lab6;

import java.awt.*;
import javax.swing.*;

/*
 * This program  had name "Paitent From V1" extend from MySimpleWindow 
 * There are 4 label , 4 textfield and 2 button 
 * 
 * @Autor Pichet Asayaporn 5930404356
 * 
 * 
 */
public class PatientFormV1 extends MySimpleWindow {
	final static int LENGTH = 15;
	protected JLabel nameL, birtdateL, weightL, heightL;
	protected JTextField nameT, birtdateT, weightT, heightT;
	protected JPanel panelLabel = new JPanel();
	protected JPanel panelText = new JPanel();
	protected JPanel panelAll = new JPanel();
	private static final long serialVersionUID = -713916976017325318L;

	public PatientFormV1(String string) {
		super(string);

	}

	public static void createAndShowGUI() {
		PatientFormV1 patientForm1 = new PatientFormV1("Patient Form V1");
		patientForm1.addComponents();
		patientForm1.setFrameFeatures();

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected void addComponents() {
		super.addComponents();

		nameL = new JLabel("Name:");
		birtdateL = new JLabel("Birthdate:");
		weightL = new JLabel("Weight (kg.):");
		heightL = new JLabel("Height (metre):");

		nameT = new JTextField(LENGTH);
		birtdateT = new JTextField(LENGTH);
		weightT = new JTextField(LENGTH);
		heightT = new JTextField(LENGTH);

		// panelLabel and setLayout by GridLayout
		panelLabel.setLayout(new GridLayout(4, 1, 0, 5));
		panelLabel.add(nameL);
		panelLabel.add(birtdateL);
		panelLabel.add(weightL);
		panelLabel.add(heightL);

		// panelText and use Gridlayout
		panelText.setLayout(new GridLayout(4, 1, 0, 5));
		panelText.add(nameT);
		panelText.add(birtdateT);
		birtdateT.setToolTipText("ex. 22.02.2000");
		panelText.add(weightT);
		panelText.add(heightT);

		// Group to panelAll and use BorderLayout
		panelAll.setLayout(new BorderLayout(10, 10));
		panelAll.add(panelLabel, BorderLayout.WEST);
		panelAll.add(panelText, BorderLayout.EAST);
		panelAll.add(super.panelmsw, BorderLayout.SOUTH);
		setContentPane(panelAll);// display panelAll
	}

}
