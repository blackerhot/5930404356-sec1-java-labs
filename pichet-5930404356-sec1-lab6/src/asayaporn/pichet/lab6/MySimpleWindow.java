package asayaporn.pichet.lab6;

import java.awt.*;
import javax.swing.*;
/*
 * This program will have 2 button 
 * 
 * @Autor Pichet Asayaporn 5930404356
 */

public class MySimpleWindow extends JFrame {

	private static final long serialVersionUID = 7937168713081791729L;
	protected JButton okBut;
	protected JButton cancelBut;
	protected JPanel panelmsw;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		MySimpleWindow msw = new MySimpleWindow("My Simple Window");
		msw.addComponents();
		msw.setFrameFeatures();

	}

	public MySimpleWindow(String string) {
		super(string);

	}

	// Method setFrameFeatures for settiing a Frame to the center of the screen

	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setVisible(true);
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // when press X then
														// program will close

	}

	protected void addComponents() {
		panelmsw = new JPanel();
		cancelBut = new JButton("Cancel");
		okBut = new JButton("OK");
		panelmsw.add(cancelBut);
		panelmsw.add(okBut);
		setContentPane(panelmsw); // dusplay panelmsw

	}

}
