package asayaporn.pichet.lab7;

import java.awt.Color;

public class TetrisShape {

	private Tetrimino shaper;
	private static int[][] coorShape ;
	private static int[][][] coordinate = new int[][][] { { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } },
			{ { 0, -1 }, { 0, 0 }, { -1, 0 }, { -1, 1 } }, { { 0, -1 }, { 0, 0 }, { 1, 0 }, { 1, 1 } },
			{ { 0, -1 }, { 0, 0 }, { 0, 1 }, { 0, 2 } }, { { -1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 1 } },
			{ { 0, 0 }, { 1, 0 }, { 0, 1 }, { 1, 1 } }, { { -1, -1 }, { 0, -1 }, { 0, 0 }, { 0, 1 } },
			{ { 1, -1 }, { 0, -1 }, { 0, 0 }, { 0, 1 } } };
	Color colors[] = {new Color(0,0,0), new Color(255, 0, 0), new Color(0, 255, 0), new Color(0, 255, 255), new Color(170, 0, 255),
					new Color(255, 255, 0), new Color(255, 165, 0), new Color(0, 0, 255) };
	Color colormatch ;
	public TetrisShape() {

		setCoorShape(new int[4][2]);
		setShape(Tetrimino.NoShape);

	}

	public void setShape(Tetrimino shape) {

		for (int j = 0; j < 4; j++) {
			for (int k = 0; k < 2; k++) {
				coorShape[j][k] = coordinate[shape.ordinal()][j][k];

			}

		}      
		colormatch = colors[shape.ordinal()];

	
		shaper=shape;

	}

	public Tetrimino getShape() {
		return shaper;
	}

	

	public static int[][][] getCoordinate() {
		return coordinate;
	}

	public int[][] getCoorShape() {
		return coorShape;
	}

	public void setCoorShape(int[][] coorShape) {
		this.coorShape = coorShape;
	}

}
