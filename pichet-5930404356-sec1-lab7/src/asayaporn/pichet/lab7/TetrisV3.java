package asayaporn.pichet.lab7;

import javax.swing.SwingUtilities;

/*
 * The program will display a block that falling down with a new color and new position of horizontal
 * 
 * @autor Pichet Asayaporn
 */
public class TetrisV3 extends TetrisV2 implements Runnable {

	private static final long serialVersionUID = -3269902413165594219L;

	public TetrisV3(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		TetrisV3 tetris = new TetrisV3("Rectangles Dropping V2");
		tetris.addComponents();
		tetris.setFrameFeatures();

	}

	TetrisPanelV3 trpanel = new TetrisPanelV3(null);

	protected void addComponents() {

		this.add(trpanel);

	}

	@Override
	public void run() {

	}
}
