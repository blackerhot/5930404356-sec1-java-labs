package asayaporn.pichet.lab7;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Toolkit;

import javax.swing.*;

public class Tetris extends JFrame {

	/**
	 * The program will display a shape of Tetris
	 * 
	 * @author Pichet Asayaporn
	 */
	private static final long serialVersionUID = -9148467452416661181L;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		Tetris tetris = new Tetris("CoE Tetris Game");
		tetris.addComponents();
		tetris.setFrameFeatures();

	}

	public Tetris(String string) {
		super(string);

	}

	// Method setFrameFeatures for setting a Frame to the center of the screen
	TetrisPanel pan = new TetrisPanel();

	protected void setFrameFeatures() {
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setVisible(true);
		pack();
		int w = getSize().width;
		int h = getSize().height;
		int x = (dim.width - w) / 2;
		int y = (dim.height - h) / 2;
		setLocation(x, y);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // when press X then
														// program will close

	}

	protected void addComponents() {

		this.add(pan);

	}

}
