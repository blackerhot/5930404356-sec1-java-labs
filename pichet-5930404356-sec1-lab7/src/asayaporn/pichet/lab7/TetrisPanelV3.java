package asayaporn.pichet.lab7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import java.util.ArrayList;
import java.util.Random;

public class TetrisPanelV3 extends TetrisPanelV2 implements Runnable {

	private static final long serialVersionUID = -5935724584193700485L;
	final int RECT_WIDTH = 60;
	final int RECT_HEIGHT = 40;
	final int SPEED = 1;
	private int x;
	private int y;
	Random rand = new Random();
	private Color colors = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
	private ArrayList<Color> notMovingRectColor;
	private ArrayList<Rectangle2D.Double> notMovingRect;
	private Thread running;

	public TetrisPanelV3(Color color) {

		running = new Thread(this);
		notMovingRect = new ArrayList<>();
		notMovingRectColor = new ArrayList<>();
		x = rand.nextInt(490);
		y = 0;
		// add a new object to arraylist
		notMovingRect.add(new Rectangle2D.Double(0, 0, 0, 0));
		notMovingRectColor.add(colors);
		running.start();

	}

	@Override
	public void run() {

		while (true) {
			// checking that Y is falling to the bottom

			if (y + SPEED + RECT_HEIGHT >= HEIGHT) {
				System.out.println("size not :  " + notMovingRect.size());
				System.out.println("size color  :  " + notMovingRect.size());
				if (notMovingRect.size() == notMovingRectColor.size()) {

					notMovingRect.add(
							new Rectangle2D.Double(x, HEIGHT - RECT_HEIGHT * 2 + 10, RECT_WIDTH * 2, RECT_HEIGHT * 2));
					notMovingRectColor.add(colors);
					colors = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
					// random new color

				}
				x = rand.nextInt(490);
				y = 0;

			} else {
				// y++ for animation
				y = y + 1;

			}

			repaint();

			try {
				// delay
				Thread.sleep(SPEED);

			} catch (InterruptedException ex) {

			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		// fill a background with White color
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, getWidth(), getHeight());

		// loop for draw animation and draw a stable block at the bottom
		// by using a size of notMovingRect

		for (int i = 0; i < notMovingRect.size(); i++) {

			g.setColor(notMovingRectColor.get(i));
			((Graphics2D) g).fill(notMovingRect.get(i));
			g.setColor(Color.darkGray);
			((Graphics2D) g).draw(notMovingRect.get(i));

			g.setColor(colors);
			g.fillRect(x, y, RECT_WIDTH * 2, RECT_HEIGHT * 2);
			g.setColor(Color.darkGray);
			g.drawRect(x, y, RECT_WIDTH * 2, RECT_HEIGHT * 2);

		}
	}

}
