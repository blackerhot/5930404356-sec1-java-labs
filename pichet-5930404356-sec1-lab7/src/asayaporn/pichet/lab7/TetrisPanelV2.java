package asayaporn.pichet.lab7;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.management.timer.Timer;

public class TetrisPanelV2 extends TetrisPanel implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6063212598876859873L;

	final int RECT_WIDTH = 50;
	final int SPEED = 10;
	int x = 0 + (int) (Math.random() * ((510 - 0) + 1));
	int y;
	private Thread running;

	public TetrisPanelV2() {

		running = new Thread(this);
		running.start();
	}

	@Override
	public void run() {

		while (true) {
			// checking Y is more than height
			if (y > HEIGHT) {
				x = 0 + (int) (Math.random() * ((510 - 0) + 1));
				y = 0;

			} else {
				y = y + 1;
			}

			repaint();
			try {

				// delay
				Thread.sleep(SPEED);

			} catch (InterruptedException ex) {

			}
		}
	}

	@Override
	public void paintComponent(Graphics g) {

		// call a super method
		super.paintComponent(g);
		// draw all background with black color
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());

		// drawrect
		g.setColor(Color.YELLOW);
		g.fillRect(x, y, RECT_WIDTH * 2, RECT_WIDTH * 2);

	}

}