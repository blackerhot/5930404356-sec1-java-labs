package asayaporn.pichet.lab7;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;

public class TetrisPanel extends JPanel {

	/**
	 * The program will display a shape of Tetris
	 * 
	 * @author Pichet Asayaporn
	 */
	private static final long serialVersionUID = -4168122350030406557L;
	final int WIDTH = 600;
	final int HEIGHT = 400;
	final int SIZE = 25;
	final int NUMHORIZON = 30;
	final int NUMVERTICAL = 20;
	protected Graphics g;
	protected int x = 0;
	protected int y = 0;


	public Dimension getPreferredSize() {
		return new Dimension(WIDTH, HEIGHT);

	}

	//Object
	TetrisShape trshape = new TetrisShape();

	@Override
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		setBackground(Color.gray);

		g.setFont(new Font("Serif", Font.BOLD, 30));
		g.setColor(Color.red);
		g.drawString("Tetris", getSize().width / 2 - 50, getSize().height / 4);

		drawing(Tetrimino.I_SHPAE, g);
		drawing(Tetrimino.Z_SHAPE, g);
		drawing(Tetrimino.T_SHPAE, g);
		drawing(Tetrimino.O_SHAPE, g);
		drawing(Tetrimino.J_SHAPE, g);
		drawing(Tetrimino.L_SHAPE, g);
		drawing(Tetrimino.S_SHAPE, g);

	}

	//method drawing a shape 
	//there are 2 parameter
	public void drawing(Tetrimino shape, Graphics g) {
		trshape.setShape(shape); // object setShpae
		int coor[][] = trshape.getCoorShape(); // pass coordinate of shape to coor 
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {

				if (j == 0)
					x = coor[i][j];
				else if (j == 1) {
					y = coor[i][j];
					
					//for set a best position
					x = 30 + (SIZE * x) + (85 * (trshape.getShape().ordinal() - 1));
					y = 180 + (SIZE * y);
				
					//drawing
					g.setColor(trshape.colormatch);
					g.fillRect(x, y, SIZE, SIZE);
					g.setColor(Color.GRAY);
					g.drawRect(x, y, SIZE, SIZE);
				}
			}
		}

	}
	

	

		

	
}
