package asayaporn.pichet.lab7;

import java.awt.Graphics;
import javax.swing.SwingUtilities;

/*
 * the program will draw a block that falling down to the bottom
 * when block left from bottom then will re-drawing a new block from top
 * 
 * @Author Pichet Asayaporn
 */
public class TetrisV2 extends Tetris {
	private static final long serialVersionUID = 2917440000779045684L;

	public TetrisV2(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		TetrisV2 tetris = new TetrisV2("TetrisV2");
		tetris.addComponents();
		tetris.setFrameFeatures();

	}

	TetrisPanelV2 trpanel = new TetrisPanelV2();

	protected void addComponents() {

		this.add(trpanel);

	}

	public void paintComponent(Graphics g) {

	}

}
