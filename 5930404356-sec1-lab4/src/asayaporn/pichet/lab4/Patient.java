package asayaporn.pichet.lab4;

import java.time.LocalDate;
import java.time.format.*;
import java.util.Locale;

public class Patient {

	DateTimeFormatter dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).withLocale(Locale.GERMAN);

	private String name;
	private LocalDate birthdate;
	private Gender gender;
	private int height;
	private double weight;

	public Patient(String passName, String passBirthdate, Gender passGender, double passWeight, int passHeight) {

		this.name = passName;
		this.birthdate = LocalDate.parse(passBirthdate, dateFormatter);
		this.gender = passGender;
		this.height = passHeight;
		this.weight = passWeight;
	}

	@Override
	public String toString() {
		return "Patient [" + name + ", " + birthdate + ", " + gender + ", " + weight + " kg. , " + height + " cm.]";
	}

	// getter and setter

	public DateTimeFormatter getNormalFormatter() {
		return dateFormatter;
	}

	public void setNormalFormatter(DateTimeFormatter dateFormatter) {
		this.dateFormatter = dateFormatter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate passBirthdate) {
		this.birthdate = passBirthdate;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

}