package asayaporn.pichet.lab4;

import java.time.LocalDate;


public class InPatient extends Patient {

	private LocalDate admitDate;
	private LocalDate dischargeDate;
	

	public LocalDate getAdmitDate() {
		return admitDate;
	}

	public void setAdmitDate(String admitDate) {

		this.admitDate = LocalDate.parse(admitDate, dateFormatter);
	}

	public LocalDate getDischargeDate() {
		return dischargeDate;
	}

	public void setDischargeDate(String dischargeDate) {
		this.dischargeDate = LocalDate.parse(dischargeDate, dateFormatter);
	}

	public InPatient(String passName, String passBirthdate, Gender passGender, double passWeight, int passHeight,
			String passAdmitDate, String passDischargeDate) {
		super(passName, passBirthdate, passGender, passWeight, passHeight);
		this.admitDate = LocalDate.parse(passAdmitDate, dateFormatter);
		this.dischargeDate = LocalDate.parse(passDischargeDate, dateFormatter);
	}

	@Override
	public String toString() {
		return "InPatient [" + getName() + ", " + getBirthdate() + ", " + getGender() + ", " + getWeight() + " kg., "
				+ getHeight() + " cm." + " admitDate=" + admitDate + ", dischargeDate = " + dischargeDate + "]";

	}

}
