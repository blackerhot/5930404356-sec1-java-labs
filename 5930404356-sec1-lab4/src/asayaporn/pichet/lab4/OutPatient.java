package asayaporn.pichet.lab4;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class OutPatient extends Patient {

	private LocalDate visitDate;
	public static String hospitalName = "Srinakarin";

	// Constructor Method
	public OutPatient(String passName, String passBirthdate, Gender passGender, double passWeight, int passHeight) {

		super(passName, passBirthdate, passGender, passWeight, passHeight);

	}

	// Overloading Constructor Method
	public OutPatient(String passName, String passBirthdate, Gender passGender, double passWeight, int passHeight,
			String passVisitDate) {

		super(passName, passBirthdate, passGender, passWeight, passHeight);
		this.visitDate = LocalDate.parse(passVisitDate, dateFormatter);

	}

	@Override
	public String toString() {
		return "OutPatient [" + getName() + ", birthdate = " + getBirthdate() + ", " + getGender() + ", " + getWeight()
				+ " kg., " + getHeight() + " cm." + " visit date = " + visitDate + "]";
	}

	public LocalDate getVisitDate() {

		return visitDate;
	}

	public void setVisitDate(String visitDate) {
		this.visitDate = LocalDate.parse(visitDate, dateFormatter);

	}

	public static String getHospitalName() {
		return hospitalName;
	}

	public static void setHospitalName(String hospitalName) {
		OutPatient.hospitalName = hospitalName;
	}

	public DateTimeFormatter getNormalFormatter() {
		return dateFormatter;
	}

	public void setNormalFormatter(DateTimeFormatter normalFormatter) {
		this.dateFormatter = normalFormatter;
	}

	// This is solution how to get daysbetween.
	public void displayDaysBetween(OutPatient piti) {
		LocalDate dateAfter = piti.getVisitDate();
		LocalDate dateBefore = this.visitDate;
		long daysBetween = ChronoUnit.DAYS.between(dateAfter, dateBefore);

		System.out.println(getName() + " visited after " + piti.getName() + " for " + daysBetween + " days.");

	}

}
