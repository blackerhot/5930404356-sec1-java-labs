package asayaporn.pichet.lab11;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
/**
*
* Class TetrisPeiceV2
* this main class will check if shape can move and draw 
* 
* @author Pichet Asayaporn 593040435-6
*/
public class TetrisPieceV2 {
	// Indicate the current Tetrismino shape of this piece
	private Tetromino pieceShape;
	private int posX, posY;
	private Color color;

	private int coordinates[][]; // coordinate of shape contains 4 points.

	/*COORDINATE_TABLE provides the coordinate all four squares that
	 * compose into one Tetromino.
	 */
	private final int[][][] COORDINATE_TABLE = new int[][][] {
			{ { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } }, // no shape
			{ { -1, 1 }, { 0, 1 }, { 0, 0 }, { 1, 0 } }, // S shape
			{ { -1, 0 }, { 0, 0 }, { 0, 1 }, { 1, 1 } }, // Z shape
			{ { -2, 0 }, { -1, 0 }, { 0, 0 }, { 1, 0 } }, // I shape
			{ { -1, 0 }, { 0, 0 }, { 1, 0 }, { 0, 1 } }, // T shape
			{ { 0, 0 }, { 1, 0 }, { 0, 1 }, { 1, 1 } }, // O shape
			{ { -1, 0 }, { 0, 0 }, { 0, 1 }, { 0, 2 } }, // L shape
			{ { -1, 0 }, { -1, 1 }, { -1, 2 }, { 0, 0 } } // J shape
	};

    public static final Color NO_SHAPE_COLOR = new Color(0, 0, 0); 			// no shape
    public static final Color S_SHAPE_COLOR = new Color(0, 255, 0); 		// S shape
    public static final Color Z_SHAPE_COLOR = new Color(255, 0, 0);   		// Z shape
    public static final Color I_SHAPE_COLOR = new Color(0, 255, 255);	    // I shape
    public static final Color T_SHAPE_COLOR = new Color(170, 0, 255); 		// T shape
    public static final Color O_SHAPE_COLOR = new Color(255, 255, 0); 		// O shape
    public static final Color L_SHAPE_COLOR = new Color(255, 165, 0); 		// L shape
    public static final Color J_SHAPE_COLOR = new Color(0, 0, 255);			// J shape
    public static final Color LINE_COLOR = Color.DARK_GRAY;	// color of the line for all tetrimino

    //TETRIMINO_COLORS provides all  color for each Tetrimino according to their order
    public final static Color TETRIMINO_COLORS[] = { NO_SHAPE_COLOR, S_SHAPE_COLOR,
		   Z_SHAPE_COLOR, I_SHAPE_COLOR, T_SHAPE_COLOR, O_SHAPE_COLOR, L_SHAPE_COLOR,
		   J_SHAPE_COLOR};

	// Shape constructor
	public TetrisPieceV2() {
		coordinates = new int[4][2];// initialize the coordinate of shape
		setShape(randomShape());
        setPosX(TetrisPanel.BOARD_WIDTH/2);
		setPosY(0);
	}

	// Set the coordinate according to shape
	public void setShapeCoordinate(Tetromino shape) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; ++j) {
				coordinates[i][j] = COORDINATE_TABLE[shape.ordinal()][i][j];
			}
		}
		pieceShape = shape;
	}


	//set shape of this piece
	public void setShape(Tetromino shape){
		setShapeCoordinate(shape);
		pieceShape = shape;
		color = TETRIMINO_COLORS[shape.ordinal()];
	}

	// set x-coordinate of point (point is indicated by index
	// value between 0 and 3.)
	protected void setX(int index, int x) {
		coordinates[index][0] = x;
	}

	// set y-coordinate of point (point is indicated by index
	// value between 0 and 3.)
	protected void setY(int index, int y) {
		coordinates[index][1] = y;
	}

	// get x coordinate of point (point is indicated by index
	// value between 0 and 3.)
	protected int getX(int index) {
		return coordinates[index][0];
	}

	// get y coordinate of point (point is indicated by index
	// value between 0 and 3.)
	protected int getY(int index) {
		return coordinates[index][1];
	}

	public Tetromino getShape() {
		return pieceShape;
	}


	/*
	 * drawTetromino(Graphics2D g2d, int curX,  int curY, Tetromino shape)
	 * curX - the position of x coordinate of the drawing shape
	 * curY - the position of y coordinate of the drawing shape
	 *        These are not pixel. To explain this, imagine the drwaing panel
	 *        as matrix with the number of column and row as BOARD_WIDTH
	 *        and BOARD_HEIGHT respectively (e.g. 30x20). Note that the index
	 *        of matrix start at 0. The curX and curY specify which square in
	 *        the matrix do you want to paint.
	 * shape -  is the shape in enum Tetromino
	 */
	public void draw(Graphics2D g2d){
		for (int i = 0; i < 4; ++i) {
            int x = posX + getX(i);
            int y = posY + getY(i);
            drawSquare(g2d, x * TetrisPanel.SQUARE_WIDTH,
                    y * TetrisPanel.SQUARE_HEIGHT);
        }
	}

	//draw sub square in each shape
    private void drawSquare(Graphics2D g2d, int x, int y)
    {
        g2d.setColor(color);
        g2d.fillRect(x, y, TetrisPanel.SQUARE_WIDTH, TetrisPanel.SQUARE_HEIGHT);
        g2d.setColor(TetrisPiece.LINE_COLOR);
        g2d.drawRect(x, y, TetrisPanel.SQUARE_WIDTH, TetrisPanel.SQUARE_HEIGHT);
    }

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	/*
	 * check if a moving piece can go any further there are two checks to make
	 * one is to check if the piece hits the bottom.
	 * The other check is to check if the piece hits other pieces
	 */
    public boolean canMoveDown(ArrayList<StaticRectangle> notMovingRect){
    	int x,y;
   	 for (int i = 0; i < 4; ++i) {
   		 x = posX + getX(i);
    		y = posY+ getY(i);
            if (x < 0 ||hitOtherPieces(notMovingRect, x, y)|| x >= TetrisPanelV2.BOARD_WIDTH  || y >= TetrisPanelV2.BOARD_HEIGHT-1)
                return false;
         
        }
   	 return true;
    }

    /*
     * Method hit other pieces which are already at the bottom
     * The method loop through all sub-rectangles to see if
     * it crashes with  the piece
     */
    private boolean hitOtherPieces(ArrayList<StaticRectangle> notMovingRect
    		 , int x, int y){
    	for(int i=0; i<notMovingRect.size(); i++){
    		if(y== notMovingRect.get(i).getRect().getY()/TetrisPanel.SQUARE_HEIGHT-1 &&
    				(x == notMovingRect.get(i).getRect().getX()/TetrisPanel.SQUARE_WIDTH))
    			return true;
    	}
    	return false;
    }

    /*
     * check if the piece can move left
     */
    public boolean canMoveLeft(ArrayList<StaticRectangle> notMovingRect){
    	int x,y;
    	 for (int i = 0; i < 4; ++i) {
    		 x = posX + getX(i);
     		y = posY + getY(i);
             if (x <= 0 ||hitOtherPieces(notMovingRect, x-1, y-1)|| x >= TetrisPanelV2.BOARD_WIDTH || y < 0 || y >= TetrisPanelV2.BOARD_HEIGHT)
                 return false;
          
         }
    	 return true;
    }

    /*
     * check if the piece can move right
     */
    public boolean canMoveRight(ArrayList<StaticRectangle> notMovingRect){
    	int x,y;
   	 for (int i = 0; i < 4; ++i) {
   		 x = posX+ getX(i);
    		y = posY+ getY(i);
            if (x < 0 ||hitOtherPieces(notMovingRect, x+1, y-1)|| x >= TetrisPanelV2.BOARD_WIDTH-1 || y < 0 || y >= TetrisPanelV2.BOARD_HEIGHT)
                return false;
         
        }
   	 return true;
    }

    /*
     * Check if the piece can rotate
     */
    public boolean canRotate(){
    	int x, y;
    	for (int i = 0; i < 4; ++i) {
    		x = posX + (-1)*getY(i);
    		y = posY + getX(i);
    		if(x > TetrisPanel.BOARD_WIDTH-1 ||
    			x <  0 || y >= TetrisPanel.BOARD_HEIGHT-1
    			)
    			return false;
    	}
    	return true;
    }

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void moveLeft() {
		posX--;
	}

	public void moveRight(){
		posX++;
	}

	public void moveDown(){
		posY++;
	}

	public void fallDown(ArrayList<StaticRectangle> notMovingRect){
		while(canMoveDown(notMovingRect)){
			posY++;
		}
	}

	public void rotate() {
		int temp;
		if(canRotate()){
			for (int i = 0; i < 4; ++i) {
				temp = getX(i);
				setX(i,(-1)*getY(i));
				setY(i,temp);
			}
		}
	}

	public void reset(){
		setShape(randomShape());
		setPosY(0);
		setPosX(TetrisPanel.BOARD_WIDTH/2);
	}

    private Tetromino randomShape(){
    	Tetromino temp[] = { Tetromino.S_SHAPE, Tetromino.Z_SHAPE, Tetromino.I_SHAPE,
    			Tetromino.T_SHAPE, Tetromino.O_SHAPE, Tetromino.L_SHAPE, Tetromino.J_SHAPE};
    	Random rand = new Random();
    	return temp[rand.nextInt(7)];
    }

	public void init() {
		setShape(randomShape());
        setPosX(TetrisPanel.BOARD_WIDTH/2);
		setPosY(0);
	}

}
