package asayaporn.pichet.lab11;

public enum Tetromino {
	NO_SHAPE, S_SHAPE, Z_SHAPE, I_SHAPE, T_SHAPE, O_SHAPE, L_SHAPE, J_SHAPE;
}
