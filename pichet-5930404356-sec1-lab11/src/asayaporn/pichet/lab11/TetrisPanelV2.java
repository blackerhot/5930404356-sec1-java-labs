package asayaporn.pichet.lab11;

import java.awt.Color;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;
/**
 *
 * Class TetrisPanelV2 extends Jpanel Runable and keylistener
 * this main class will check a keypress and draw a notmoving rect
 * 
 * @author Pichet Asayaporn 593040435-6
 */
public class TetrisPanelV2 extends JPanel implements Runnable, KeyListener{

	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 200, HEIGHT = 400;
	public static final int BOARD_WIDTH = 10, BOARD_HEIGHT = 20;
	public static final int SLEEP_TIME = 500;
	public static final int SQUARE_WIDTH = WIDTH/BOARD_WIDTH;
    public static final int SQUARE_HEIGHT = HEIGHT/BOARD_HEIGHT;


    protected int MOVE_LEFT = KeyEvent.VK_LEFT;
	protected int MOVE_RIGHT = KeyEvent.VK_RIGHT;
	protected int FALL_DOWN = KeyEvent.VK_DOWN;
	protected int ROTATE = KeyEvent.VK_UP;

    protected final int MAX_RECT = 600;

    ArrayList<StaticRectangle> notMovingRect =
    		new ArrayList<StaticRectangle>();

    private Thread running;
	protected TetrisPieceV2 curPiece;

	public TetrisPanelV2() {
		super();
		setBackground(Color.LIGHT_GRAY);
		setSize(new Dimension(WIDTH,  HEIGHT));

		curPiece = new TetrisPieceV2();
        setFocusable(true);
		addKeyListener(this);

		running=new Thread(this);
		running.start();
	}


	@Override
	public void paint(Graphics g){
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        drawNotMovingRects(g2d);
        curPiece.draw(g2d);
	}

	/**
     * Runs the game.
     */
    public void run() {
        while (true) {
        	if(curPiece.canMoveDown(notMovingRect)){
        		curPiece.moveDown();
        	}else{
        		// dump squares to be drawn later
        		pieceNotMoving();

        		//Generate new piece
        		curPiece.reset();
        	}
        	repaint();

            try {
                Thread.sleep(SLEEP_TIME);
            } catch (InterruptedException ignore) {
                // Do nothing
            }

        }
    }

	public Dimension getPreferredSize() {
	    return new Dimension(WIDTH, HEIGHT);
	}

	protected void pieceNotMoving(){
		for (int i = 0; i < 4; ++i) {
			int x = curPiece.getPosX() + curPiece.getX(i);
            int y = curPiece.getPosY() + curPiece.getY(i);
            notMovingRect.add(new StaticRectangle(new Rectangle2D.Double(x*SQUARE_WIDTH,
          		   y*SQUARE_HEIGHT, SQUARE_WIDTH, SQUARE_HEIGHT), curPiece.getColor()));

    	}
	}


	@Override
	//method keyPressed for checking all action of key
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==this.FALL_DOWN){
			
			this.curPiece.fallDown(notMovingRect);
		
		}else if(e.getKeyCode()==this.MOVE_LEFT){
			if(this.curPiece.canMoveLeft(notMovingRect))
			{
			this.curPiece.moveLeft();
	
			
			}
		}else if(e.getKeyCode()==this.MOVE_RIGHT){
			if(	this.curPiece.canMoveRight(notMovingRect))
			{
			this.curPiece.moveRight();
			}
			
		}else if(e.getKeyCode()==this.ROTATE){
			this.curPiece.rotate();
			
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		//Do nothing
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		//Do nothing
	}

	protected void drawNotMovingRects(Graphics2D g2d){
        for(int i=0; i<notMovingRect.size(); i++){
			g2d.setColor(notMovingRect.get(i).getColor());
			g2d.fill(notMovingRect.get(i).getRect());
			g2d.setColor(Color.DARK_GRAY);
			g2d.draw(notMovingRect.get(i).getRect());
        }
		repaint();
	}
}
