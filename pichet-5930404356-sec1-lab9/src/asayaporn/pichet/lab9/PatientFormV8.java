package asayaporn.pichet.lab9;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

/**
 * This program extends from PateintFormV7
 * then add Accelerator to JMenuItem
 * and set Mnemonic to all JMenu and JMenuItem
 * 
 * @author Pichet Asayaporn 593040435-6
 *
 */
public class PatientFormV8 extends PatientFormV7 {

	
	private static final long serialVersionUID = 7727475401876359737L;

	public PatientFormV8(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	public static void createAndShowGUI() {
		PatientFormV8 patientForm8 = new PatientFormV8("Patient Form V8");
		patientForm8.addComponents();
		patientForm8.addListeners();
		patientForm8.setFrameFeatures();

	}

	@Override
	public void addComponents() {
		super.addComponents();
		
		//set all Key Accelerator to JmenuItem and sup-MenuItem
		
		super.menuNew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		super.menuOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		super.menuSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		super.menuExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK));

		super.cBlue.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK));
		super.cRed.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		super.cGreen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		super.cCustom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));

		super.s16.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6, ActionEvent.CTRL_MASK));
		super.s20.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, ActionEvent.CTRL_MASK));
		super.s24.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, ActionEvent.CTRL_MASK));
		super.sCustom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.CTRL_MASK));

		//set all Mnemonic keys to menu , menuItem
		super.menuFile.setMnemonic('F');
		super.menuNew.setMnemonic('N');
		super.menuOpen.setMnemonic('O');
		super.menuSave.setMnemonic('S');
		super.menuExit.setMnemonic('X');
		super.menuConfig.setMnemonic('C');
		super.supColor.setMnemonic('L');
		super.cBlue.setMnemonic('B');
		super.cRed.setMnemonic('R');
		super.cGreen.setMnemonic('G');
		super.cCustom.setMnemonic('U');
		super.supSize.setMnemonic('Z');
		super.s16.setMnemonic('6');
		super.s20.setMnemonic('0');
		super.s24.setMnemonic('4');
		super.sCustom.setMnemonic('M');

	}

	

}
