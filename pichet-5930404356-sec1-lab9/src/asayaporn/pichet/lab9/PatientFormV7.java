package asayaporn.pichet.lab9;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import asayaporn.pichet.lab8.PatientFormV6;

/**
 * This program will extends from PatientFormV6
 * and add 2 JSlider and action of both
 * 
 * 
 * @author Pichet Asayaporn 593040435-6
 *
 */
public class PatientFormV7 extends PatientFormV6 implements ChangeListener {


	private static final long serialVersionUID = 524926119687525028L;
	protected JPanel bloodPanel, sliderPanel;
	protected JLabel topNum, botNum;
	protected JPanel numPanel;
	protected JSlider topSlider, botSlider;
	protected JButton okNewBut;

	//Constructor for adding name
	public PatientFormV7(String string) {
		super(string);
		// TODO Auto-generated constructor stub
	}

	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	
	public static void createAndShowGUI() {
		PatientFormV7 patientForm7 = new PatientFormV7("Patient Form V7");
		patientForm7.addComponents();
		patientForm7.addListeners();
		patientForm7.setFrameFeatures();

	}

	@Override
	public void addComponents() {
		super.addComponents();
		
		sliderPanel = new JPanel();
		bloodPanel = new JPanel();
		numPanel = new JPanel();
		
		//initialize JSlider min 0 max 200 and 100 for mid
		topSlider = new JSlider(0, 200, 100);
		botSlider = new JSlider(0, 200, 100);
		
		//create Border with a line and Title
		bloodPanel.setBorder(new TitledBorder("Blood Pressure"));

		//setting Major , Minor , show Label and name
		topSlider.setName("Top number");
		topSlider.setMajorTickSpacing(50);
		topSlider.setMinorTickSpacing(10);
		topSlider.setLabelTable(topSlider.createStandardLabels(50));
		topSlider.setPaintLabels(true);
		topSlider.setPaintTicks(true);

		botSlider.setName("Bottom number");
		botSlider.setMajorTickSpacing(50);
		botSlider.setMinorTickSpacing(10);
		botSlider.setLabelTable(botSlider.createStandardLabels(50));
		botSlider.setPaintLabels(true);
		botSlider.setPaintTicks(true);
		
		//set GridLayout of 2 JSlider
		sliderPanel.setLayout(new GridLayout(2, 1));
		sliderPanel.add(topSlider);
		sliderPanel.add(botSlider);
		topNum = new JLabel("Top number: ");
		botNum = new JLabel("Bottom number: ");

		//setting panelLabel
		numPanel.setLayout(new GridLayout(2, 1));
		numPanel.add(topNum);
		numPanel.add(botNum);

		//Split Label and slider Panel
		bloodPanel.setLayout(new BorderLayout());
		bloodPanel.add(numPanel, BorderLayout.WEST);
		bloodPanel.add(sliderPanel, BorderLayout.EAST);
		
		// add to super class
		super.panelBot.add(bloodPanel, BorderLayout.NORTH);

		//add new ok button
		super.panelmsw.remove(okBut);
		okNewBut = new JButton("OK");
		super.panelmsw.add(okNewBut);
		

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		super.actionPerformed(e);
		Object command = e.getSource();
	
		// checking type each of object then set action for each of them
		if (command == this.okNewBut) {
			
			//show message dialog
			JOptionPane.showMessageDialog(null,
					" Name = " + nameT.getText() + " Birthdate = " + birtdateT.getText() + " Weight = "
							+ weightT.getText() + " Height = " + heightT.getText() + "\n" + " Gender = "
							+ (maleRadio.isSelected() ? "Male" : "Female") + "\n" + " Address = "
							+ textAddress.getText() + "\n" + " Type = " + combo.getSelectedItem() 
							+ "\nBlood Pressure : "+topSlider.getName()+ " = " + topSlider.getValue() 
							+", "+ botSlider.getName() +" = " +botSlider.getValue());
		
		}


	}

	@Override
	protected void addListeners() {
		super.addListeners();

		//Slider use ChangListener
		botSlider.addChangeListener(this);
		topSlider.addChangeListener(this);
		okBut.removeActionListener(this);//remover old okBut
		okNewBut.addActionListener(this);//add new okBut to panel button
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider source = (JSlider) e.getSource();
		//checking source is adjusting
		if (!source.getValueIsAdjusting()) {

			//show message dialog
			JOptionPane.showMessageDialog(null, source.getName() + " of Blood Pressure is " + source.getValue());

		}
	}
}
