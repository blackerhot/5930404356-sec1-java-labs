package asayaporn.pichet.lab2;

public class MoneyChanger {
	public static void main(String[] args) {

		int cost = Integer.parseInt(args[0]);

		if (cost == 1000) {
			System.out.println("You give 1000 for the cost as " + cost + " Baht.");
			System.out.println("No change.");
		}

		else if (cost > 0 && cost < 1000) {
			System.out.println("You give 1000 for the cost as " + cost + " Baht.");
			cost = 1000 - cost;
			System.out.println("You will receive exchange as " + cost + " Baht.");

			if (cost >= 500) {

				System.out.print("1 of 500 bank note; ");
				cost = cost - 500;

				if (cost >= 100) {

					System.out.print((cost / 100) + " of 100 bank note; ");
					cost = cost - ((cost / 100) * 100);
				}
				if (cost >= 50) {

					System.out.print((cost / 50) + " of 50 bank note; ");
					cost = cost - ((cost / 50) * 50);
				}
				if (cost >= 20) {

					System.out.print((cost / 20) + " of 20 bank note; ");
					cost = cost - ((cost / 20) * 20);
				}

			}

			else if (cost >= 100) {

				System.out.print((cost / 100) + " of 100 bank note; ");
				cost = cost - ((cost / 100) * 100);

				if (cost >= 50) {

					System.out.print((cost / 50) + " of 50 bank note; ");
					cost = cost - ((cost / 50) * 50);
				}

				if (cost >= 20) {

					System.out.print((cost / 20) + " of 20 bank note; ");
					cost = cost - ((cost / 20) * 20);
				}

			}

			else if (cost >= 50) {

				System.out.print((cost / 50) + " of 50 bank note; ");
				cost = cost - ((cost / 50) * 50);

				if (cost >= 20) {

					System.out.print((cost / 20) + " of 20 bank note; ");
					cost = cost - ((cost / 20) * 20);

				}
			}

			else if (cost >= 20) {

				System.out.print((cost / 20) + " of 20 bank note; ");
				cost = cost - ((cost / 20) * 20);

			}

		}
	}
}
