package asayaporn.pichet.lab2;

public class Patient2 {
	public static void main(String[] args) {

		if (args.length != 4) {

			System.err.println("Patient <name> <gender> <weight> <height>");
			return;
		}

		String name = args[0];
		String gender = args[1];
		float weight = Float.parseFloat(args[2]);
		int height = Integer.parseInt(args[3]);

		if (gender.equalsIgnoreCase("Male")) {

			if (weight >= 0 && height >= 0) {

				System.out.println("This patient name is " + name);
				System.out.println("His weight is " + weight + " kg. and height is " + height + " cm.");

			} else if (height < 0 && weight < 0) {
				System.out.println("This patient name is " + name);
				System.out.println("Weight must be non-negative");
			} else if (height < 0) {
				System.out.println("This patient name is " + name);
				System.out.println("Height must be non-negative");
			} else if (weight < 0) {
				System.out.println("This patient name is " + name);
				System.out.println("Weight must be non-negative");
			}

		}

		else if (gender.equalsIgnoreCase("Female")) {

			if (weight >= 0 && height >= 0) {

				System.out.println("This patient name is " + name);
				System.out.println("Her weight is " + weight + " kg. and height is " + height + " cm.");

			} else if (height < 0 && weight < 0) {
				System.out.println("This patient name is " + name);
				System.out.println("Weight must be non-negative");
			} else if (height < 0) {
				System.out.println("This patient name is " + name);
				System.out.println("Height must be non-negative");
			} else if (weight < 0) {
				System.out.println("This patient name is " + name);
				System.out.println("Weight must be non-negative");
			}

		}

		else {
			System.out.println("This patient name is " + name);
			System.out.println("Please enter gender as only Male or Female");
		}

	}
}