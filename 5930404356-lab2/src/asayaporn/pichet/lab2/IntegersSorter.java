package asayaporn.pichet.lab2;

import java.util.Arrays; // import for sorting array.

public class IntegersSorter {

	public static void main(String[] args) {

		int i;
		int[] arynum;
		arynum = new int[args.length];

		if (args.length == 1) {

			System.err.println("IntegersSorter <the number of integers to sort> <i1> <i2> ... <in> ");
			return;

		}

		else {

			// Show the args befor sorting
			System.out.print("Before sorting:[");
			System.out.print(Integer.parseInt(args[1]));
			for (i = 2; i < args.length; i++) {
				System.out.print(", " + Integer.parseInt(args[i]));
			}
			System.out.println("]");

			// convert args to Array
			for (i = 1; i < args.length; i++) {
				arynum[i] = Integer.parseInt(args[i]);
			}

			// sort array
			Arrays.sort(arynum);

			// show the array after sorting
			System.out.print("After sorting:[");
			System.out.print(arynum[1]);
			for (i = 2; i < args.length; i++) {
				System.out.print(", " + arynum[i]);
			}
			System.out.println("]");

		}

	}

}
