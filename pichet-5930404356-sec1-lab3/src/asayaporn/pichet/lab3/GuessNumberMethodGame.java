package asayaporn.pichet.lab3;

import java.util.Scanner;

public class GuessNumberMethodGame {

	static int remainingGuess = 7;
	static int answer;

	public static void genAnswer() {

		answer = 0 + (int) (Math.random() * ((100 - 0) + 1));
	}

	public static void playGame() {
		while (remainingGuess >= 1) {
			System.out.println("Number of remaining guess is " + remainingGuess);

			Scanner num = new Scanner(System.in);
			System.out.print("Enter a guess: ");

			int number = num.nextInt();

			if (number == answer) {

				System.out.println("Correct!");
				return;

			} else if (number < answer) {

				System.out.println("Higher!");
			}

			else if (number > answer) {

				System.out.println("Lower!");
			}

			remainingGuess--;
		}
		System.out.println("You ran out of guesses. The number was " + answer);
	}

	public static void main(String[] args) {

		genAnswer();
		playGame();

	}

}