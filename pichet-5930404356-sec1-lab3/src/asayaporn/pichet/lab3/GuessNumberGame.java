package asayaporn.pichet.lab3;

import java.util.Scanner;

public class GuessNumberGame {
	static int min = 0;
	static int max = 100;

	public static void main(String[] args) {

		int i = 7;
		int random = min + (int) (Math.random() * ((max - min) + 1));

		while (i >= 1) {

			System.out.println("Number of remaining guess is " + i--);

			Scanner input = new Scanner(System.in);

			System.out.print("Enter a guess: ");

			int number = input.nextInt();

			if (number == random) {

				System.out.println("Correct!");
				System.exit(0);

			} else if (number < random) {

				System.out.println("Higher!");
			}

			else if (number > random) {

				System.out.println("Lower!");
			}

		}
		System.out.println("You ran out of guesses. The number was " + random);
	}

}