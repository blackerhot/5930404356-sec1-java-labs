package asayaporn.pichet.lab3;

import java.util.Scanner;

public class GuessNumberGameV2 {

	static int remainingGuess;
	static int answer;
	static int min;
	static int max;

	public static void genAnswer() {

		answer = min + (int) (Math.random() * ((max - min) + 1));
	}

	public static void playGame() {

		while (remainingGuess >= 1) {
			System.out.println("Number of remaining guess is " + remainingGuess);

			Scanner num = new Scanner(System.in);
			System.out.print("Enter a guess: ");

			int number = num.nextInt();

			if (number >= min && number <= max) {
				remainingGuess--;
				if (number == answer) {

					System.out.println("Correct!");
					return;

				} else if (number < answer) {

					System.out.println("Higher!");
				}

				else if (number > answer) {

					System.out.println("Lower!");
				}

			} else {
				System.out.println("Numbers must be in " + min + " and " + max);

			}

		}
		System.out.println("You ran out of guesses. The number was " + answer);

	}

	public static void main(String[] args) {

		String condition;
		do {

			Scanner input = new Scanner(System.in);
			System.out.print("Enter min and max of random numbers: ");
			min = input.nextInt();
			max = input.nextInt();

			genAnswer();

			System.out.print("Enter the number possible guess : ");
			remainingGuess = input.nextInt();

			playGame();

			Scanner inputcondi = new Scanner(System.in);
			System.out.print("Want to play agian? (\"Y\" or \"y\") : ");
			condition = inputcondi.nextLine();

		} while (condition.equalsIgnoreCase("y"));

	}

}
